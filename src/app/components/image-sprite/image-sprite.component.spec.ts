import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ImageSpriteComponent } from './image-sprite.component';

describe('ImageSpriteComponent', () => {
  let component: ImageSpriteComponent;
  let fixture: ComponentFixture<ImageSpriteComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ImageSpriteComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ImageSpriteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
