import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HelloWorldComponent } from './components/hello-world/hello-world.component';
import { ImageSpriteComponent } from './components/image-sprite/image-sprite.component';

@NgModule({
  declarations: [AppComponent, HelloWorldComponent, ImageSpriteComponent],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
